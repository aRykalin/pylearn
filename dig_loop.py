def median(sequence):
    sequence = sorted(sequence)
    if len(sequence) % 2 != 0:
        mid = sequence[(len(sequence) / 2)]
    else:
        mid1 = (len(sequence) / 2) - 1
        mid2 = (len(sequence) / 2)
        m1 = sequence[mid1]
        m2 = sequence[mid2]
        mid = (m1 + m2) / 2.0
    return mid

print median([1,1,2])
print median([7,3,1,4])

print median([11,33,556,6,7,8])