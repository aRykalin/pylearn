new_versions = {
    'Nucleus-Portal': '5.1.0',
    'Nucleus-Source': '5.1.0',
    'nucleus-utils': '2.0.0'
}

old_versions = {
    'Nucleus-Portal': '5.0.0',
    'Nucleus-Source': '5.0.0',
    'nucleus-utils': '2.0.0'
}

src_branch = {
    'Nucleus-Portal': 'trunk',
    'Nucleus-Source': 'trunk',
    'nucleus-utils': 'trunk'
}

dst_branch = {
    'Nucleus-Portal': 'branches/release-' + new_versions['Nucleus-Portal'],
    'Nucleus-Source': 'branches/release-' + new_versions['Nucleus-Source'],
    'nucleus-utils': 'branches/release-' + new_versions['nucleus-utils']
}

svn = 'https://dev-test-cm-standby01.carpathia.com/scm/svn/ticketing'
artifacts = ['Nucleus-Portal', 'Nucleus-Source', 'nucleus-utils']

for a in artifacts:
    if new_versions[a] != old_versions[a]:
        print "Artifact: ", a, "New version: ", new_versions[a], "Old version: ", old_versions[a]
        print "Source branch: ", svn + src_branch[a], "Dest branch: ", svn + dst_branch[a]=======
#import subprocess

artifacts = ['Portal-Source', 'Nucleus-Source', 'nucleus-utils', 'carpathia-portal-menu', 'carpathia-devops-ansible', 'OpenAM-Customization', 'OpenAM-12.0.2', 'carpathia-commons', 'carpathia-nucleus']

new_versions = {
    'Portal-Source': '5.1.0RC',
    'Nucleus-Source': '5.1.0RC',
    'nucleus-utils': '2.0.0',
    'carpathia-portal-menu': '2.1.0RC',
    'carpathia-devops-ansible': '1.1.0RC',
    'OpenAM-Customization': '1.1.0RC',
    'carpathia-commons': '2.0.0RC',
    'carpathia-nucleus': '2.1.0RC',
    'OpenAM-12.0.2': '1.1.0RC'
}

old_versions = {
    'Portal-Source': '5.1.0',
    'Nucleus-Source': '5.1.0',
    'nucleus-utils': '2.0.0',
    'carpathia-portal-menu': '2.1.0',
    'carpathia-devops-ansible': '1.1.0',
    'OpenAM-Customization': '1.1.0',
    'carpathia-commons': '2.0.0',
    'carpathia-nucleus': '2.1.0',
    'OpenAM-12.0.2': '1.1.0'
}

versions_properties = {
    'carpathia-nucleus': 'com.carpathia.carpathia-nucleus-permissions.version',
    'carpathia-nucleus': 'com.carpathia.carpathia-nucleus.version',
    'carpathia-portal-menu': 'com.carpathia.carpathia-portal-menu.version',
    'Portal-Source': 'com.carpathia.carpathia-test.version',
    'nucleus-utils': 'com.carpathia.carpathia-utils.version',
    'carpathia-commons': 'com.carpathia.commons.version',
    'em7api': 'com.carpathia.em7api.version',
    'Nucleus-Source': 'com.carpathia.nucleus-local.version'
}

svn_base = 'https://dev-test-cm-standby01.carpathia.com/scm/svn/ticketing'
svn_revision = '32071'
src_branch = 'trunk'
dst_branch = 'branches/release-'
jira_task = "CTT-64727"
version_sufix = "-SNAPSHOT"
message = "Creating RC branch"

class Artifact(object):
    """docstring for Artifact"""
    jira_task = jira_task
    svn_revision = svn_revision
    svn_base = svn_base
    src_branch = src_branch
    dst_branch = dst_branch
    version_sufix = version_sufix
    def __init__(self, name):
        super(Artifact, self).__init__()
        self.name = name
        self.new_version = new_versions[name]
        self.old_version = old_versions[name]
        self.svn_url = svn_base + '/' + name
        if src_branch == 'trunk':
            self.src_branch = src_branch
        else:    
            self.src_branch = branch + self.old_version
        self.dst_branch = dst_branch + self.new_version
        self.src = self.svn_url + '/' + self.src_branch
        self.dst = self.svn_url + '/' + self.dst_branch
    def svn_copy(self):
        print "svn copy -r%s -m \"%s: %s %s\" %s %s" % (svn_revision, jira_task, message, self.new_version, self.src, self.dst)
    def svn_co(self):
        print "svn co %s %s" % (self.dst, self.name)
    def mvn_set_version(self):
        print "cd %s" % (self.name)
        print "mvn versions:set -DnewVersion=%s%s -DgenerateBackupPoms=false" % (self.new_version, self.version_sufix)
        print "cd .."  
    def svn_cleanup(self):
        print "svn del -m \"%s\" %s" % ('CM: deleting junk branch', self.dst)
    def svn_commit(self):
        print "cd %s" % (self.name)
        print "svn ci -m \"%s: %s %s\"" % (jira_task, message, self.new_version)
        print "svn info"
        print "cd .."  
    def incr_version(self):
        l = self.old_version.split(".")
        l[1] = int(l[1]) + 1
        l[1] = str(l[1])
        self.new_trunk_version = '.'.join(l)
        print "#Update trunk version of %s to %s" % (self.name, self.new_trunk_version)
        print "svn co %s %s" % (self.src, self.name)
        print "cd %s" % (self.name)
        print "mvn versions:set -DnewVersion=%s%s -DgenerateBackupPoms=false" % (self.new_trunk_version, self.version_sufix)
        print "cd .." 
    def check_deps(self,dep_list,dep_list_versions):
        for d in dep_list:
            print "%s : checking version %s of %s dependecy" % (self.name, dep_list_versions[d],d)


for a in artifacts:
    if new_versions[a] != old_versions[a]:
        artifact = Artifact(a)
        artifact.svn_copy()
        artifact.svn_co()
        artifact.mvn_set_version()

print "exit 1"
print "#Commit after check:"
for a in artifacts:
    if new_versions[a] != old_versions[a]:
        artifact = Artifact(a)
        artifact.svn_commit()

print "exit 1"
print "#Cleanup, run after check:"
for a in artifacts:
    if new_versions[a] != old_versions[a]:
        artifact = Artifact(a)
        artifact.svn_cleanup()

print "exit 1"
print "#Increase trunk versions"
for a in artifacts:
    if new_versions[a] != old_versions[a]:
        artifact = Artifact(a)
        message = "Commiting new trunk versions"
        artifact.svn_commit()
        artifact.incr_version()
        artifact.check_deps(artifacts,old_versions)

#Just example
#portal = Artifact("Portal-Source")
#portal.jira_task = "CTT-666"
#print "jira task for Portal-Source: %s" % (portal.jira_task)

#TO DO: exec svn and mvn from script itself:
#cmd = "echo svn base is: %s" % (svn_base)
#cmd = "svn info %s " % (svn_base)
#process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
#          stderr=subprocess.PIPE, universal_newlines=True)
#out, err = process.communicate()
#print out
#https://docs.python.org/2/library/subprocess.html

#TO DO: make for in threads
#TO DO: change deps from olv ver to new by checking every artifact version var

print "#Check versions after"
print "find ./ -iname '*pom*xml'|xargs grep -r 0.0-SNAPSHOT"
print 'find ./ -name pom.xml |xargs grep "com.carpathia.*version.*com.carpathia.*version"'
