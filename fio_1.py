#test
my_list = [i**2 for i in range(1,11)]

my_file = open("output.txt", "r+")

for item in my_list:
	my_file.write(str(item) + "\n")
my_file.close()

my_file = open("output.txt", "r")
print my_file.read()
my_file.close()

my_file = open("text.txt", "r")
print my_file.readline()
print my_file.readline()
print my_file.readline()
my_file.close()

# Open the file for reading
read_file = open("text.txt", "r")

# Use a second file handler to open the file for writing
write_file = open("text.txt", "w")
# Write to the file
write_file.write("Not closing files is VERY BAD.\nha ha ha \nha"  )

write_file.close()

# Try to read from the file
print read_file.read()
write_file.close()

with open("text.txt", "w") as textfile:
	textfile.write("Success!")

f = open("text.txt")
print f.closed
f.close()
print f.closed

with open("text.txt", "w") as my_file:
	my_file.write("haha!")
if not my_file.closed:
	my_file.close()
print my_file.closed
