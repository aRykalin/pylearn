new_versions = {
    'Nucleus-Portal': '5.1.0',
    'Nucleus-Source': '5.1.0',
    'nucleus-utils': '2.0.0'
}

old_versions = {
    'Nucleus-Portal': '5.0.0',
    'Nucleus-Source': '5.0.0',
    'nucleus-utils': '2.0.0'
}

src_branch = {
    'Nucleus-Portal': 'trunk',
    'Nucleus-Source': 'trunk',
    'nucleus-utils': 'trunk'
}

dst_branch = {
    'Nucleus-Portal': 'branches/release-' + new_versions['Nucleus-Portal'],
    'Nucleus-Source': 'branches/release-' + new_versions['Nucleus-Source'],
    'nucleus-utils': 'branches/release-' + new_versions['nucleus-utils']
}
svn = 'https://svn:123/svn/'
artifacts = ['Nucleus-Portal', 'Nucleus-Source', 'nucleus-utils']

for a in artifacts:
    if new_versions[a] != old_versions[a]:
        print "Artifact: ", a, "New version: ", new_versions[a], "Old version: ", old_versions[a]
        print "Source branch: ", svn + src_branch[a], "Dest branch: ", svn + dst_branch[a]