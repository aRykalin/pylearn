from random import randint

def create_board(board):
    for x in range(0, 5):
        board.append(["O"] * 5)

def print_board(board):
    for row in board:
        print " ".join(row)

def random_row(board):
    return randint(0, len(board) - 1)

def random_col(board):
    return randint(0, len(board[0]) - 1)


def set_my_ship(board):
    my_ship_row = int(raw_input("Set Row:")) - 1
    my_ship_col = int(raw_input("Set Col:")) - 1
    board[my_ship_row][my_ship_col] = 'V'

def show_boards():
    print "Enemy board:"
    print_board(enemyBoard)
    print "My board:"
    print_board(myBoard)

enemyBoard = []
myBoard = []
create_board(enemyBoard)
create_board(myBoard)
enemy_ship_row = random_row(enemyBoard)
enemy_ship_col = random_col(enemyBoard)

set_my_ship(myBoard)
show_boards()

turn = 0
for turn in range(100):
    print "Turn", turn + 1
    guess_row = int(raw_input("Guess Row:")) - 1
    guess_col = int(raw_input("Guess Col:")) - 1
    hit = 'x'
    while hit != 'unique':
        print 'Aiming....'
        hit_row = random_row(myBoard)
        hit_col = random_col(myBoard)
        if myBoard[hit_row][hit_col] == 'X':
            print 'oops, I hit that already, trying again'
        else:
            print 'I hit to row: ', hit_row, ' and col: ', hit_col
            hit = 'unique'

    # Write your code below!
    if guess_col == enemy_ship_col and guess_row == enemy_ship_row:
        print "Congratulations! You sank my battleship!"
        enemyBoard[enemy_ship_row][enemy_ship_col] = "V"
        show_boards()
        break
    elif myBoard[hit_row][hit_col] == 'V':
        print "Mu ha ha! Your ship is on fire!"
        myBoard[hit_row][hit_col] = "P"
        show_boards()
        break
    else:
        if guess_row not in range(5) or guess_col not in range(5):
            print "Oops, that's not even in the ocean."
        elif enemyBoard[guess_row][guess_col] == 'X':
            print "You guessed that one already."
        else:
            print "You missed my battleship!"
            enemyBoard[guess_row][guess_col] = "X"
            myBoard[hit_row][hit_col] = "X"
        if turn == 100:
            print "Game Over, out of turns"
            enemyBoard[enemy_ship_row][enemy_ship_col] = "V"

    show_boards()
    turn += 1