one = 0b1
two = 0b10
three = 0b11
four = 0b100
five = 0b101
six = 0b110
seven = 0b111
eight = 0b1000
nine = 0b1001
ten = 0b1010
eleven = 0b1011
twelve = 0b1100

print bin(0b1110 & 0b101) == 0b100
print bin(0b1110 | 0b101) == 0b1111

def check_bit4(input):
    mask = 0b1000
    desired = input & mask
    if desired > 0:
        return "on"
    else:
        return "off"

a = 0b10111011
mask = 0b100
print bin(a | mask)

a = 0b11101110
mask = 0b11111111
print bin(a ^ mask)

a = 0b101 
# Tenth bit mask
mask = (0b1 << 9)  # One less than ten 
print bin(a ^ mask)

def flip_bit(number, n):
	mask = (0b1 << (n - 1))
	result = number ^ mask
	return bin(result)

