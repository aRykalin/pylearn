grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades):
	for g in grades:
		print g

def grades_sum(scores):
	f = 0
	for s in scores:
		f += s
	return f

def grades_average(grades):
	sum = grades_sum(grades)
	avg = grades_sum(grades) / float(len(grades))
	return avg

def grades_variance(scores):
	average = grades_average(scores)
	variance = 0
	for score in scores:
		variance += (average - score) ** 2
	total_v = variance / len(scores)
	return total_v

def grades_std_deviation(variance):	
	return variance ** 0.5

print_grades(grades)
print grades_sum(grades)
print grades_average(grades)
print grades_variance(grades)
print grades_std_deviation(grades_variance(grades))