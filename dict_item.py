d = {
    "Name": "Guido",
    "Age": 56,
    "BDFL": True
}
print d.items()

my_dict = {
    'Portal-Source': '5.0.0RC',
    'Nucleus-Source': '5.0.0RC',
    'nucleus-utils': '2.0.0RC',
    'carpathia-portal-menu': '2.0.0RC'
}

print my_dict.items()
print my_dict.keys()
print my_dict.values()

d = { "name": "Eric", "age": 26 }
for key in d:
    print key, d[key]

